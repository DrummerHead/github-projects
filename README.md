# GitHub projects

This is a toy project where you can log in to GitHub, view the repos who have an associated [project](https://help.github.com/articles/about-project-boards/) and then be able to see the first project of that repo.

## Running

```
yarn
yarn start
```

And your browser should show you `http://localhost:3000/`

[This is a video of the project running, in case you don't want to install it](https://youtu.be/nkvGpS74svA)

## Project details

This project's folder structure is [grouped by features](https://reactjs.org/docs/faq-structure.html#grouping-by-features-or-routes), this is useful if there are associated reducers, type annotations or helper code associated to the feature.

The starting point comes from [create-react-app](https://github.com/facebook/create-react-app)

EDIT: This was a hiring excercise. [You can see the premise at this image](https://i.imgur.com/CDdAxSN.png)

### Libraries used

* axios as HTTP client
* react-router-dom for routing
* styled-components for handling CSS
* markdown-to-jsx to parse Cards' text

### Improvements

Given the time constraints, this working MVP has a lot of room for improvement.

With more time, this improvements should be made:

* Use OAuth with a back-end instead of basic authentication (security issue)
* Handling error states. What would happen if you enter `/dashboard` without being logged in
* Abstracting Form components for general Form use
* Show all the projects associated with a Repo, not just the first one
* More animations to improve user understanding of application state change
* Handle more viewport sizes
* Have an "ORG mode" where you can see all the projects of an ORG (instead of the projects of a users's repos)
* if there's a plan to grow the project, use Redux for state management (and then replace `react-router-dom` with `redux-little-router`)
* if there's a plan to grow the project, use Flow for static typing
