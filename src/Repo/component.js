import React from 'react';
import axios from 'axios';
import styled from 'styled-components';
import { Redirect } from 'react-router-dom';
import axiosConfig from '../axiosConfig';

const RepoCard = styled.div`
  margin: 1em;
  background-color: #eee;
  border: 1px solid #ccc;
  border-radius: 0.25em;
  padding: 0.1em 1.25em;
  flex: 1 0 auto;
  transition: opacity 250ms ease-in;
  opacity: ${props => (props.enabled ? '1' : '.4')};
  cursor: ${props => (props.enabled ? 'pointer' : 'not-allowed')};
`;

class Repo extends React.Component {
  state = {
    projects: [],
    hasFetched: false,
    hasProjects: false,
    hasBeenClicked: false,
  };

  componentDidMount() {
    axios
      .get(
        `https://api.github.com/repos/${this.props.loginState.username}/${
          this.props.data.name
        }/projects`,
        axiosConfig(
          this.props.loginState.username,
          this.props.loginState.password
        )
      )
      .then(res => {
        this.setState({
          projects: res.data,
          hasProjects: res.data.length > 0,
          hasFetched: true,
        });
      })
      .catch(err => {
        console.info('Error:');
        console.error(err);
        this.setState({
          hasProjects: false,
          hasFetched: true,
        });
      });
  }

  handleClick = () => {
    this.setState({ hasBeenClicked: true });
  };

  render() {
    if (this.state.hasProjects && this.state.hasBeenClicked) {
      return <Redirect to={`/dashboard/${this.state.projects[0].id}/`} />;
    }

    return (
      <RepoCard enabled={this.state.hasProjects} onClick={this.handleClick}>
        <h3>{this.props.data.name}</h3>
        <p>{this.props.data.description}</p>
      </RepoCard>
    );
  }
}

export default Repo;
