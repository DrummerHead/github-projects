import React from 'react';
import axios from 'axios';
import styled from 'styled-components';
import Markdown from 'markdown-to-jsx';

import axiosConfig from '../axiosConfig';

const Columns = styled.div`
  display: flex;
  flex-flow: row nowrap;
  margin: 0 -1em;
`;

const Column = styled.div`
  flex: 1 0;
  padding: 0.2em 1em;
  margin: 1em;
  border: 0.1em #e1e4e8 solid;
  border-radius: 1em;
  background-color: #eff1f3;
`;

const Card = styled.div`
  padding: 1em;
  margin: 1.25em 0;
  border: 1px #e1e4e8 solid !important;
  border-radius: 0.5em;
  background-color: #fff;
  box-shadow: 0 1px 1px rgba(27, 31, 35, 0.1);
`;

class Project extends React.Component {
  state = {
    columns: [],
    hasFetched: false,
  };

  componentDidMount() {
    axios
      .get(
        `https://api.github.com/projects/${
          this.props.match.params.project
        }/columns`,
        axiosConfig(
          this.props.loginState.username,
          this.props.loginState.password
        )
      )
      .then(res => {
        this.setState({ columns: res.data, hasFetched: true });
        res.data.forEach(column =>
          axios
            .get(
              column.cards_url,
              axiosConfig(
                this.props.loginState.username,
                this.props.loginState.password
              )
            )
            .then(innerRes => {
              this.setState(prevState => ({
                columns: prevState.columns.map(
                  col =>
                    col.id === column.id
                      ? { ...col, ...{ cards: innerRes.data } }
                      : col
                ),
              }));
            })
        );
      })
      .catch(err => {
        console.info('Error:');
        console.error(err);
      });
  }

  render() {
    return (
      <div>
        <h1>Project</h1>
        <Columns>
          {this.state.hasFetched &&
            this.state.columns.map(column => (
              <Column key={column.id}>
                <h2>{column.name}</h2>
                {column.cards &&
                  column.cards.map(card => (
                    <Card key={card.id}>
                      <Markdown>{card.note}</Markdown>
                    </Card>
                  ))}
              </Column>
            ))}
        </Columns>
      </div>
    );
  }
}

export default Project;
