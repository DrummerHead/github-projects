const axiosConfig = (username, password) => ({
  headers: {
    accept: 'application/vnd.github.inertia-preview+json',
  },
  auth: { username, password },
});

export default axiosConfig;
