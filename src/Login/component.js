import React from 'react';
import { Redirect } from 'react-router-dom';
import styled from 'styled-components';

const LoginPage = styled.div`
  margin: 28vh auto 0;
  width: 28em;
`;

const Label = styled.label`
  display: flex;
  flex-flow: row nowrap;
  margin: 1em 0;

  & span {
    margin: 0 1em 0 0;
  }
  & input {
    flex: 1 0 auto;
  }
`;

const Button = styled.button`
  margin: 0 0 1rem 0;
  padding: 0.85em 1em;
  border: 1px solid transparent;
  appearance: none;
  border-radius: 3px;
  font-size: 0.9rem;
  line-height: 1;
  text-align: center;
  background-color: #1779ba;
  color: #fefefe;
  cursor: pointer;
`;

const ErrorMessage = styled.span`
  margin: 0 1em;
  color: red;
`;

class Login extends React.Component {
  state = {
    username: '',
    password: '',
  };

  handleInput = inputType => ev =>
    this.setState({ [inputType]: ev.target.value });

  handleSubmit = ev => {
    ev.preventDefault();
    this.props.auth(this.state.username, this.state.password);
  };

  render() {
    if (this.props.loginSuccess === true) {
      return <Redirect to="/dashboard/" />;
    }

    return (
      <LoginPage>
        <form onSubmit={this.handleSubmit}>
          <h1>Enter your Github credentials:</h1>
          <Label>
            <span>User:</span>
            <input
              type="text"
              value={this.state.username}
              onChange={this.handleInput('username')}
            />
          </Label>
          <Label>
            <span>Password:</span>
            <input
              type="password"
              value={this.state.password}
              onChange={this.handleInput('password')}
            />
          </Label>
          <Button type="submit">Log in</Button>

          {this.props.loginSuccess === false && (
            <ErrorMessage>Login Error, try again please</ErrorMessage>
          )}
        </form>
      </LoginPage>
    );
  }
}

export default Login;
