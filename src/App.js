import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import axios from 'axios';
import styled from 'styled-components';

import axiosConfig from './axiosConfig';
import Dashboard from './Dashboard/component';
import Login from './Login/component';
import Project from './Project/component';

const Main = styled.main`
  padding: 0.25em 1.5em;
  font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Helvetica, Arial,
    sans-serif;

  & h1 {
    font-weigth: 3em;
    font-weight: 100;
  }
`;

class App extends React.Component {
  state = {
    login: {
      success: null,
      username: '',
      password: '',
    },
    repos: [],
  };

  auth = (username, password) => {
    axios
      .get('https://api.github.com/user/repos', axiosConfig(username, password))
      .then(res => {
        this.setState({
          login: {
            success: true,
            username,
            password,
          },
          repos: res.data,
        });
      })
      .catch(err => {
        console.info('Error:');
        console.error(err);
        this.setState({
          login: {
            success: false,
          },
        });
      });
  };

  render() {
    return (
      <Router>
        <Main>
          <Switch>
            <Route
              exact
              path="/"
              render={props => (
                <Login
                  auth={this.auth}
                  loginSuccess={this.state.login.success}
                />
              )}
            />
            <Route
              exact
              path="/dashboard/"
              render={props => (
                <Dashboard
                  repos={this.state.repos}
                  loginState={this.state.login}
                />
              )}
            />
            <Route
              path="/dashboard/:project"
              render={props => (
                <Project {...props} loginState={this.state.login} />
              )}
            />
          </Switch>
        </Main>
      </Router>
    );
  }
}

export default App;
