import React from 'react';
import styled from 'styled-components';

import Repo from '../Repo/component';

const Repos = styled.div`
  display: flex;
  flex-flow: row wrap;
  margin: 0 -0.75em;
`;

const Dashboard = props => (
  <div>
    <h1>Dashboard &#8212; Your repositories</h1>
    <p>Click on a repository to see its projects</p>
    <Repos>
      {props.repos.map(project => (
        <Repo data={project} key={project.id} loginState={props.loginState} />
      ))}
    </Repos>
  </div>
);

export default Dashboard;
